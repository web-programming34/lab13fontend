import { ref } from "vue";
import { defineStore } from "pinia";
import { useCustomerStore } from "./customer";

export const useConfirmUserStore = defineStore("confirm", () => {
  const isShow = ref(false);
  const customer = useCustomerStore();
  const Id = ref<number>();
  function Delete(id: number) {
    if (Id.value !== -1) {
      customer.deleteCustomer(id);
      Id.value = -1;
      isShow.value = false;
    }
  }
  function Confirm(id: number) {
    isShow.value = true;
    Id.value = id;
  }

  return { isShow, Delete, Confirm, Id };
});
